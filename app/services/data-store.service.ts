import { Injectable } from 'angular2/core';

import { Person } from 'app/interfaces/person';
import { ApiService } from 'app/services/api.service';

@Injectable()

var personFoundInvokeList: Array<Function> = [];
export class DataStoreService {

	private apiService: ApiService;

	public setApi(apiService: ApiService) {
		this.apiService = apiService;
	}

	private raisePersonFound(person: Person) {
		personFoundInvokeList.forEach(callback => {
			callback(person);
		});
	}

	public onPersonFound(callback: (person: Person) => void) {
		personFoundInvokeList.push(callback);
	}

	searchByField(field: string) {
		this.apiService.getPerson(field).then(person => this.raisePersonFound(person));
	}

}