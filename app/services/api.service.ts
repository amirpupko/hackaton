import { Injectable } from 'angular2/core';

import { Person } from 'app/interfaces/person';


const LANGUAGES_SCORES_FROM_DATE = "https://soluto-tech-analyzer-api.herokuapp.com/byUserFromDate/";
const TOTAL_SCORES_BY_USER_URL = "https://soluto-tech-analyzer-api.herokuapp.com/byUser/";

var personFoundInvokeList: Array<Function> = [];
var languagesAndLibraries = {};

/* JS METHODS DECLARATIONS */
declare function http_get(url: string): Promise;

function formatDateForUrl(date: Date) {
  var yyyy = date.getFullYear().toString();
  var mm = (date.getMonth() + 1).toString();
  var dd = date.getDate().toString();
  return yyyy + (mm[1] ? mm : "0" + mm[0]) + (dd[1] ? dd : "0" + dd[0]);
}

/* NAMESPACE FUNCTIONALITIES */
function adaptUserModelToPerson(name, userModel) {

  var email = name;
  if (name.indexOf("@") == -1) {
    email += "@soluto.com";
  }

  var languages = userModel.languages;
  var person = {
    personalDetails: {
      email: email,
      avatarUrl: "unknown"
    },
    techInfos: []
  }

  for (var languageName in languages) {
    if (languages.hasOwnProperty(languageName)) {
      var languageModel = languages[languageName];
      var newLanguageInfo = {
        language: languageName,
        codelines: languageModel.lines,
        libraryScores: []
      }

      if (languageModel.hasOwnProperty('libraries')) {
        var modelLibraries = languageModel.libraries;
        for (var libraryName in modelLibraries) {
          if (modelLibraries.hasOwnProperty(libraryName)) {
            var newLibrary = {
              name: libraryName,
              scores: [{
                score: modelLibraries[libraryName].score,
                timestamp: new Date()
              }]
            }

            newLanguageInfo.libraryScores.push(newLibrary);
          }
        }
      }

      person.techInfos.push(newLanguageInfo);
    }
  }

  return person;
}
@Injectable({
})


export class ApiService {

  getLanguagesScoresOfUserFromDate(user: string, date: Date) {

    return http_get(LANGUAGES_SCORES_FROM_DATE + user + "/" + formatDateForUrl(date));
  }

  getPersonNames() {
    return new Promise((resolve, reject) => {
      http_get("https://soluto-tech-analyzer-api.herokuapp.com/users").then(result => {
        resolve(result);
      });
    });
  }

  getPerson(name: string) {
    return new Promise((resolve, reject) => {
      http_get(TOTAL_SCORES_BY_USER_URL + name).then(userModel => {
        var person = adaptUserModelToPerson(name, userModel);
        resolve(person);
      });
    });
  }

  getTechnologyNames() {
    return new Promise((resolve, reject) => {
      http_get("https://soluto-tech-analyzer-api.herokuapp.com/techList").then(result => {
        result.languages.forEach(x => {
          languagesAndLibraries[x.toLowerCase()] = "language";
        });
        result.libraries.forEach(x => {
          languagesAndLibraries[x.toLowerCase()] = "library";
        });
        resolve(result.languages.concat(result.libraries));
      });
    });
  }

  getTechnology(techName: string) {
    return new Promise((resolve, reject) => {
      var url = languagesAndLibraries[techName.toLowerCase()];
      http_get("https://soluto-tech-analyzer-api.herokuapp.com/byTech/" + url + "/" + techName).then(result => {
        if (url == "language") {
          console.log(1);
          function compare(b, a) {
            if (a.codeLines < b.codeLines)
              return -1;
            else if (a.codeLines > b.codeLines)
              return 1;
            else
              return 0;
          }

          var totalUsage = Object.keys(result).reduce((acc, curr) => {
            return acc + result[curr];
          }, 0);

          var contributers = Object.keys(result).map(x => {
            return {
              name: x,
              codeLines: result[x],
              percentage: Math.round(result[x] / totalUsage * 100),
            }
          });

          contributers.sort(compare);
          resolve({
            name: techName,
            topContributors: contributers.slice(0, 3),
          });
        }
        else {
          function compare(b, a) {
            if (a.usage < b.usage)
              return -1;
            else if (a.usage > b.usage)
              return 1;
            else
              return 0;
          }

          var totalUsage = Object.keys(result).reduce((acc, curr) => {
            return acc + result[curr];
          }, 0);

          var contributers = Object.keys(result).map(x => {
            return {
              name: x,
              usage: result[x],
              percentage: Math.round(result[x] / totalUsage * 100),
            }
          });
          contributers.sort(compare);
          resolve({
            name: techName,
            topContributors: contributers.slice(0, 3),
          });
        }
      });
    });
  }

  getAllTeamTechnologyStats() {
    console.log("Getting all team statistics");

    return new Promise((resolve, reject) => {
      resolve(ALLTEAM_TECHNOLOGIES_MOCK);
    });
  }
}

var TECHNOLOGY_MOCK = [
  {
    name: "rx",
    introducedAt: new Date(),
    introducedBy: "Yshay",
    topContributors: [
      {
        name: "Yshay",
        // codeLines: 0,
        usage: 234;
        percentage: 15;
      },
      {
        name: "Zahi",
        // codeLines: 0,
        usage: 111;
        percentage: 12;
      },
      {
        name: "Yossi",
        // codeLines: 0,
        usage: 34;
        percentage: 3;
      }
    ]
  },
  {
    name: "Java",
    introducedAt: new Date(),
    introducedBy: "Moshe",
    topContributors: [
      {
        name: "Moshe",
        codeLines: 1645,
        // usage: 234;
        // percentage: 15;
      }
    ]
  }
]


var PERSONS_MOCK = [
  {
    personalDetails: {
      fullName: "p1",
      joinedAt: new Date(),
      team: 'team 8',
      avatarUrl: '/some-url/should-be/here.jpg'
    },
    techInfos: [{
      language: "JS",
      codelines: 45679871,
      libraryScores: [
        {
          name: 'Backend',
          scores: [{
            timestamp: new Date(),
            score: 15
          }, {
              timestamp: new Date(),
              score: 15
            }]
        }, {
          name: 'Node',
          scores: [{
            timestamp: new Date(),
            score: 15
          }, {
              timestamp: new Date(),
              score: 15
            }]
        },
      ]
    }, {
        language: "HTML",
        codelines: 2314564,
        libraryScores: [
          {
            name: 'Backend',
            scores: [{
              timestamp: new Date(),
              score: 15
            }, {
                timestamp: new Date(),
                score: 15
              }]
          }, {
            name: 'Node',
            scores: [{
              timestamp: new Date(),
              score: 15
            }, {
                timestamp: new Date(),
                score: 15
              }]
          },
        ]
      }]
  }, {
    personalDetails: {
      fullName: "p2",
      joinedAt: new Date(),
      team: 'team 8',
      avatarUrl: '/some-url/should-be/here.jpg'
    },
    techInfos: [{
      language: "HTML",
      codelines: 45679871,
      libraryScores: [
        {
          name: 'Backend',
          scores: [{
            timestamp: new Date(),
            score: 15
          }, {
              timestamp: new Date(),
              score: 15
            }]
        }
      ]
    }]
  }, {
    personalDetails: {
      fullName: "p3",
      joinedAt: new Date(),
      team: 'team 8',
      avatarUrl: '/some-url/should-be/here.jpg'
    },
    techInfos: [{
      language: "CSS",
      codelines: 1124,
      libraryScores: [
        {
          name: 'Backend',
          scores: [{
            timestamp: new Date(),
            score: 15
          }, {
              timestamp: new Date(),
              score: 15
            }]
        }
      ]
    }]
  }, {
    personalDetails: {
      fullName: "p4",
      joinedAt: new Date(),
      team: 'team 8',
      avatarUrl: '/some-url/should-be/here.jpg'
    },
    techInfos: [{
      codelines: 112342,
      libraryScores: [
        {
          name: 'Backend',
          scores: [{
            timestamp: new Date(),
            score: 15
          }, {
              timestamp: new Date(),
              score: 15
            }]
        }
      ]
    }]
  }

]

var ALLTEAM_TECHNOLOGIES_MOCK = [{
  name: 'AllTeam',
  changesByTechnology: [
    {
      name: 'C#';
      codeLines: 1287;
      numOfteamMembers: 32;
      percentage: 29;
    },
    {
      name: 'JS';
      codeLines: 953;
      numOfteamMembers: 25;
      percentage: 27;
    },
    {
      name: 'Java';
      codeLines: 454;
      numOfteamMembers: 20;
      percentage: 22;
    },
    {
      name: 'Swift';
      codeLines: 234;
      numOfteamMembers: 7;
      percentage: 15;
    }
	}]
]
