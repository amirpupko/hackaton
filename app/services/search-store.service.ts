import {Injectable} from 'angular2/core';

// Scoped Withing this source. Usable for all search store services.
var searchAcceptedInvokeList: Array<Function> = [];

@Injectable()
export class SearchStoreService {

	searchAccepted(searchField: String){
		if (typeof searchAcceptedInvokeList != "undefined"){
			searchAcceptedInvokeList.forEach(toInvoke => {
				toInvoke(searchField);
			});
		}
	}

	addListener(callback: (str: string) => void){
		searchAcceptedInvokeList.push(callback);
	}
}