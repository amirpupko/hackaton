export interface TechnologyContributor {
	name: string;
	codeLines: number;
	usage: number;
	percentage: number;
}
