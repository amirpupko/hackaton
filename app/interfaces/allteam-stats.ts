import { TechnologyChangesStats } from 'app/interfaces/technology-changes-stats'

export interface AllTeamStats {
    name: string;
    changesByTechnology: Array<TechnologyChangesStats>;
}
