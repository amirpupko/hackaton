import { ScoreItem } from 'app/interfaces/score-item';

export interface LibraryScore {
	name: string;
	scores: Array<ScoreItem>;
}