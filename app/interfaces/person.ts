import { PersonalDetails } from 'app/interfaces/personal-details';
import { TechInfo } from 'app/interfaces/tech-info';

export interface Person {
	personalDetails: PersonalDetails;
	techInfos: TechInfo[];
}