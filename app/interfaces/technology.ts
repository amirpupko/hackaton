import { TechnologyContributor } from 'app/interfaces/technology-contributor'

export interface Technology {
	name: string;
	introducedAt: Date;
	introducedBy: string;
	topContributors: Array<TechnologyContributor>;
}
