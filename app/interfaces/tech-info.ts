import { LibraryScore } from 'app/interfaces/library-score'

export interface TechInfo {
	language: string;
	codelines: number;
	libraryScores: Array<LibraryScore>;
}