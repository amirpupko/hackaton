export interface ScoreItem {
	timestamp: Date;
	score: number;
}