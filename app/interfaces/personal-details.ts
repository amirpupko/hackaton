export interface PersonalDetails {
	email: string;
	avatarUrl: string;
}