export interface TechnologyChangesStats {
    name: string;
    codeLines: number;
    numOfteamMembers: number;
    percentage: number;
}
