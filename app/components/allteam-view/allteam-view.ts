import { Component, OnInit } from 'angular2/core'

import { LibraryScore } from 'app/interfaces/library-score';
import { AllTeamStats } from 'app/interfaces/allteam-stats';
import { TechnologyChangesStats } from 'app/interfaces/technology-changes-stats';

import { AllTeamStatsPieChartViewComponent } from 'app/components/allteam-piechart-stats/allteam-piechart-stats';

@Component({
	selector: 't8-allteam-view',
	templateUrl: '/app/components/allteam-view/allteam-view.html',
	inputs: ['allTeamStats'],
	directives: [AllTeamStatsPieChartViewComponent]
})

export class AllTeamViewComponent implements OnInit {

	public allTeamStats;

	ngOnInit(){
		console.log("All Team Stats Obj: " + this.allTeamStats);
	}

	isDisplay() {
		return typeof this.allTeamStats != "undefined";
	}

}
