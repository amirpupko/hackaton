import { Component, OnInit } from 'angular2/core'

import { LibraryScore } from 'app/interfaces/library-score';
import { AllTeamStats } from 'app/interfaces/allteam-stats';
import { TechnologyChangesStats } from 'app/interfaces/technology-changes-stats'

declare function charts_render_score_summary_pie(containerId: string, title: string, seriesName: string, data: any);
declare function barcharts_render_simple_barchart(containerId: string, inputTitle: string, inputXAxis: any, inputYAxis: any, dataSeries: any, dataName: string));

@Component({
	selector: 't8-allteam-piechart-stats',
	templateUrl: '/app/components/allteam-piechart-stats/allteam-piechart-stats.html',
	inputs: ['allTeamStats']
})

export class AllTeamStatsPieChartViewComponent implements OnInit {

	public allTeamStats;
	public pieChartStats: Array<LibraryScore> = [];

	ngOnInit(){
		console.log("All Team Stats Obj: " + this.allTeamStats);

		if (this.allTeamStats !== undefined)
		{
			console.log("All Team Stats Obj: " + this.allTeamStats);

			var techStatsDistribution = [];
			var technologiesNames = [];
			var teamMemberPerTech = [];

			for (var idx = 0; idx < this.allTeamStats[0].changesByTechnology.length; ++idx){
				var techStatsItem = {
					name: this.allTeamStats[0].changesByTechnology[idx].name,
					y: this.allTeamStats[0].changesByTechnology[idx].percentage
				}

				techStatsDistribution.push(techStatsItem);

				technologiesNames.push(this.allTeamStats[0].changesByTechnology[idx].name);
				teamMemberPerTech.push(this.allTeamStats[0].changesByTechnology[idx].numOfteamMembers);
			}

			charts_render_score_summary_pie(
				"allteam-stats-per-tech",
				"Technology usage out of this month usage",
				"Percentage out of changes",
				techStatsDistribution);

			barcharts_render_simple_barchart(
				"allteam-teammembers-per-tech",
				"Team Members using each technology",
				technologiesNames,
				[0,5,10,15,20,25,30,35],
				teamMemberPerTech,
				"Team Members"
			);
		}
	}
}
