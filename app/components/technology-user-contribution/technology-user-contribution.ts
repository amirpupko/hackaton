import { Component, OnInit } from 'angular2/core'
import { TechnologyContributor } from 'app/interfaces/technology-contributor';

@Component({
	selector: 't8-technology-user-contribution',
	templateUrl: '/app/components/technology-user-contribution/technology-user-contribution.html',
	inputs: ['topContributors'],
})

export class TechnologyUserContributionComponent implements OnInit {

	public topContributors: Array<TechnologyContributor>;

	constructor() { }

	ngOnInit(){
	}
}
