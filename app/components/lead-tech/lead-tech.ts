import { Component, OnInit } from 'angular2/core'

import { ApiService } from 'app/services/api.service';
import { LibraryScore } from 'app/interfaces/library-score';
import { Person } from 'app/interfaces/person'
import { TechInfo } from 'app/interfaces/tech-info';
import { PersonalDetails } from 'app/interfaces/personal-details';

declare function charts_render_tech_performance(containerId: string, title: string, data: any);

@Component({
	selector: 't8-lead-tech',
	templateUrl: '/app/components/lead-tech/lead-tech.html',
	inputs: ['techInfo', 'personalDetails'],
	providers: [ApiService]
})

export class LeadTechComponent {
	public techInfo: TechInfo;
	public personalDetails: PersonalDetails;

	public trends: Array<LibraryScore> = [];

	public isLoadingTrends = false;

	constructor(private _apiService: ApiService){ }

	ngOnInit(){

		var uname = this.personalDetails.email;
		if (uname.indexOf("@") != -1) {
			uname = uname.split('@')[0];
		}

		var date = new Date();
		date.setDate(date.getDate() - 14); // two weeks

		this._apiService.getLanguagesScoresOfUserFromDate(uname, date).then(result => {
			this.isLoadingTrends = true;

			var defaultTimestamp = new Date();

			var trends = [];
			var language = result.languages[this.techInfo.language];
			if (typeof language != "undefined"){
				var libraries = language.libraries;
				for (var libName in libraries){
					var library = libraries[libName];

					trends.push({
						name: libName,
						scores: [{
							score: library.score,
							timestamp: defaultTimestamp
						}]
					})
				}
			}

			trends.sort((a, b) =>{
				return a.scores[0].score - b.scores[0].score;
			});

			this.trends = trends;
			this.isLoadingTrends = false;

		});

		var orderedScores = this.techInfo.libraryScores.sort((a, b) => {
			// This assumes that the scores timestampes are ordered
			return a.scores[a.scores.length - 1].score - 
				   b.scores[b.scores.length - 1].score;
		});

		for (var idx = 0; idx < orderedScores.length && idx < 3; ++idx){
			this.trends.push(orderedScores[idx]);
		}

		charts_render_tech_performance(
			"overall-tech-performance",
			"Overall " + this.techInfo.language + " performance",
			this.trends);
	}
}