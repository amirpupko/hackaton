import { Component, OnInit } from 'angular2/core'

import { TechnologyDetailsComponent } from 'app/components/technology-details/technology-details';
import { TechnologyUserContributionComponent } from 'app/components/technology-user-contribution/technology-user-contribution';
import { SubscribeComponent } from 'app/components/subscribe/subscribe';

@Component({
  selector: 't8-technology-view',
  templateUrl: '/app/components/technology-view/technology-view.html',
  inputs: ['technology'],
  directives: [TechnologyDetailsComponent, TechnologyUserContributionComponent, SubscribeComponent]
})

export class TechnologyViewComponent implements OnInit {

  public technology: Technology;
  public subscribeText: string;

  constructor() {
  }

  ngOnInit() {
	}

  isDisplay() {
    if (typeof this.technology != "undefined") {
      this.subscribeText = "Follow " + this.technology.name + " evolution";
    }
    return typeof this.technology != "undefined";
  }
}
