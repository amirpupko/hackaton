import { Component, OnInit } from 'angular2/core'

import { Person } from 'app/interfaces/person';

import { PersonalDetailsComponent } from 'app/components/personal-details/personal-details';
import { TechInfoComponent } from 'app/components/tech-info/tech-info';
import { SubscribeComponent } from 'app/components/subscribe/subscribe';

@Component({
	selector: 't8-single-person-view',
	templateUrl: '/app/components/single-person-view/single-person-view.html',
	inputs: ['person'],
	directives: [PersonalDetailsComponent, TechInfoComponent, SubscribeComponent]
})

export class SinglePersonViewComponent implements OnInit {

	public person: Person;
	public subscribeText: string;

	constructor() { }

	ngOnInit(){ }

	isDisplay() {
		if (typeof this.person != "undefined") {
			var uname = this.person.personalDetails.email;
			if (uname.indexOf("@") != -1) {
				uname = uname.split("@")[0];
			}
			this.subscribeText = "Follow " + uname + "'s latest trends";
		}
		return typeof this.person != "undefined";
	}
}
