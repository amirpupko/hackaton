import { Component, OnInit } from 'angular2/core';

import { SearchComponent } from 'app/components/search/search'
import { SinglePersonViewComponent } from 'app/components/single-person-view/single-person-view';
import { TechnologyViewComponent } from 'app/components/technology-view/technology-view';
import { AllTeamViewComponent } from 'app/components/allteam-view/allteam-view';

import { Person } from 'app/interfaces/person';
import { Technology } from 'app/interfaces/technology';
import { AllTeamStats } from '../../interfaces/allteam-stats';
import { SearchStoreService } from 'app/services/search-store.service';
import { ApiService } from 'app/services/api.service';

@Component({
  selector: 't8-app',
  templateUrl: '/app/components/app/app.html',
  directives: [SearchComponent, SinglePersonViewComponent, TechnologyViewComponent, AllTeamViewComponent],
  providers: [SearchStoreService, ApiService]
})

export class AppComponent implements OnInit {

  public viewPerson: Person;
  public technology: Technology;
  public viewAllTeam: AllTeamStats;

  constructor(private _searchStoreService: SearchStoreService, private _apiService: ApiService)
  { }

  ngOnInit() {

    this._searchStoreService.addListener(value => {

        // TODO: Cache names to classify value

        delete this.technology;
        delete this.viewPerson;
        delete this.viewAllTeam;

        if (value.toLowerCase() === "allteam"){
          var allTeamPromise = this._apiService.getAllTeamTechnologyStats().then(allTeam => {
            this.viewAllTeam = allTeam;
          })
        } else {
          var personPromise = this._apiService.getPerson(value).then(person => {
            this.viewPerson = person;
          })

          var personPromise = this._apiService.getTechnology(value).then(tech => {
            this.technology = tech;
          })
        }
    });
  }
}
