import { Component, OnInit } from 'angular2/core'

@Component({
	selector: 't8-technology-details',
	templateUrl: '/app/components/technology-details/technology-details.html',
	inputs: ['technology']
})

export class TechnologyDetailsComponent implements OnInit {

	public technology: Technology;

	constructor() { }

	ngOnInit(){ }
}
