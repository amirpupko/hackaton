import { Component, OnInit } from 'angular2/core'

@Component({
	selector: 't8-subscribe',
	templateUrl: '/app/components/subscribe/subscribe.html',
	inputs: ['text']
})

export class SubscribeComponent implements OnInit {

	public text: string;

	constructor() { }

	ngOnInit(){ }
}
