import { Component, OnInit } from 'angular2/core';

import { SearchStoreService } from 'app/services/search-store.service';
import { ApiService } from 'app/services/api.service';

@Component({
	selector: 't8-search',
	templateUrl: '/app/components/search/search.html',
	styleUrls: ['/app/components/search/search.css'],
	providers: [SearchStoreService, ApiService]
})

export class SearchComponent implements OnInit {

	public searchSuggestions: Array<string> = [];
	public searchField: string;

	constructor(private _searchStoreService: SearchStoreService, private _apiService: ApiService){ }

	ngOnInit(){
		this._apiService.getPersonNames().then(names => {
			names.forEach(name =>{
				if (this.searchSuggestions.indexOf(name) == -1){
					this.searchSuggestions.push(name);
				}
			})
		})

		this._apiService.getTechnologyNames().then(names => {
			names.forEach(name =>{
				if (this.searchSuggestions.indexOf(name) == -1){
					this.searchSuggestions.push(name);
				}
			})
		})

		// Add the all team option
		if (this.searchSuggestions.indexOf('AllTeam') == -1){
			this.searchSuggestions.push('AllTeam');
		}
	}

	searchAccepted(){
		this._searchStoreService.searchAccepted(this.searchField);
	}
}
