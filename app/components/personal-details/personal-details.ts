import { Component, OnInit } from 'angular2/core'
import { ApiService } from 'app/services/api.service'

import { PersonalDetails } from 'app/interfaces/personal-details';

declare function charts_render_score_summary_pie(containerId: string, title: string, seriesName: string, data: any);

@Component({
	selector: 't8-personal-details',
	templateUrl: '/app/components/personal-details/personal-details.html',
	inputs: ['personalDetails'],
	providers: [ApiService]
})

export class PersonalDetailsComponent {
	public personalDetails: PersonalDetails;

	public isLoadingData = false;

	public availableTimeRanges = [
		"Week",
		"Month",
		"Half a year"
	]

	public lastRefreshedTimeRange: string;

	constructor(private _apiService: ApiService){ }

	refreshTimeRange(timeRange: string){

		if (this.isLoadingData || 
			this.lastRefreshedTimeRange == timeRange ||
			this.availableTimeRanges.indexOf(timeRange) == -1){
			console.error("No such time range: " + timeRange);
			return;
		}

		this.isLoadingData = true;

		var uname = this.personalDetails.email;
		if (uname.indexOf("@") != -1){
			uname = uname.split('@')[0];
		}

		var decreaseValue = 7;
		if (timeRange.toLowerCase() === "month"){
			decreaseValue = 30;
		} else if (timeRange.toLowerCase() == "half a year") {
			decreaseValue = 180;
		}
		var date = new Date();
		date.setDate(date.getDate() - decreaseValue);

		this._apiService.getLanguagesScoresOfUserFromDate(uname, date).then(result => {
			var languages = result.languages;
			var languagesScores = [
			]

			for (var langName in languages) {
				if (languages.hasOwnProperty(langName)) {
					

					var language = languages[langName];
					if (language.hasOwnProperty("libraries")){
						for (var libraryName in language.libraries){
							if (language.libraries.hasOwnProperty(libraryName)){
								var langItem = {
									name: libraryName,
									y: language.libraries[libraryName].score
								}
								languagesScores.push(langItem);
							}
						}
					}
				}
			}

			charts_render_score_summary_pie(
				"score-summary-pie-container",
				"Libraries Usage over the " + timeRange,
				"Score",
				languagesScores
			);

			this.isLoadingData = false;
			this.lastRefreshedTimeRange = timeRange;
		});
	}

	ngOnInit(){		
		this.refreshTimeRange("Week");
	}
}


/* HOW IT WAS

import { Component, OnInit } from 'angular2/core'
import { ApiService } from 'app/services/api.service'

import { PersonalDetails } from 'app/interfaces/personal-details';

declare function charts_render_score_summary_pie(containerId: string, title: string, seriesName: string, data: any);

@Component({
	selector: 't8-personal-details',
	templateUrl: '/app/components/personal-details/personal-details.html',
	inputs: ['personalDetails'],
	providers: [ApiService]
})

export class PersonalDetailsComponent {
	public personalDetails: PersonalDetails;

	public isLoadingData = false;

	public availableTimeRanges = [
		"Last Week",
		"Last Month",
		"Half a year"
	]

	public lastRefreshedTimeRange: string;

	constructor(private _apiService: ApiService){ }

	refreshTimeRange(timeRange: string){

		if (this.isLoadingData || 
			this.lastRefreshedTimeRange == timeRange ||
			this.availableTimeRanges.indexOf(timeRange) == -1){
			console.error("No such time range: " + timeRange);
			return;
		}

		this.isLoadingData = true;

		var uname = this.personalDetails.email;
		if (uname.indexOf("@") != -1){
			uname = uname.split('@')[0];
		}

		var decreaseValue = 7;
		if (timeRange.toLowerCase() === "last month"){
			decreaseValue = 30;
		} else if (timeRange.toLowerCase() == "half a year") {
			decreaseValue = 180;
		}
		var date = new Date();
		date.setDate(date.getDate() - decreaseValue);

		this._apiService.getLanguagesScoresOfUserFromDate(uname, date).then(result => {
			var languages = result.languages;
			var languagesScores = [
			]

			for (var langName in languages) {
				if (languages.hasOwnProperty(langName)) {
					var langItem = {
						name: langName,
						y: 0
					}

					var language = languages[langName];
					if (language.hasOwnProperty("libraries")){
						for (var libraryName in language.libraries){
							if (language.libraries.hasOwnProperty(libraryName)){
								langItem.y += language.libraries[libraryName].score;
							}
						}
					}

					languagesScores.push(langItem);
				}
			}

			charts_render_score_summary_pie(
				"score-summary-pie-container",
				"Totals Scores, " + timeRange,
				"Score",
				languagesScores
			);

			this.isLoadingData = false;
			this.lastRefreshedTimeRange = timeRange;
		});
	}

	ngOnInit(){		
		this.refreshTimeRange("Last Month");
	}
}

*/