import { Component, OnInit } from 'angular2/core';

import { TechInfo } from 'app/interfaces/tech-info';
import { LibraryScore } from 'app/interfaces/library-score';
import { LeadTechComponent } from 'app/components/lead-tech/lead-tech';
import { PersonalDetails } from 'app/interfaces/personal-details';

@Component({
	selector: 't8-tech-info',
	templateUrl: '/app/components/tech-info/tech-info.html',
	directives: [LeadTechComponent],
	inputs: ['techInfos', 'personalDetails']
})

export class TechInfoComponent implements OnInit {
	techInfos: TechInfo[];
	personalDetails: PersonalDetails;

	ngOnInit(){
		this.techInfos = this.techInfos.sort((a, b) => {

			var tops = ['js', 'javascript'];

			if (tops.indexOf(a.language.toLowerCase()) != -1){
				return -1;
			} else if (tops.indexOf(b.language.toLowerCase()) != -1){
				return 1;
			}

			return 0;
		});
	}
}