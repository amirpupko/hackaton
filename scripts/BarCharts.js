function barcharts_render_simple_barchart(containerId, inputTitle, inputXAxis, inputYAxis, dataSeries, dataName)
    {
        new Highcharts.Chart({
            chart: {
                renderTo: document.getElementById(containerId),
                type: 'bar'
            },
            title: {
                text: inputTitle
            },
        xAxis: {
            categories: inputXAxis,
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: null,
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: null
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: {
            enabled: false,
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 80,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [{
            name: dataName,
            data: dataSeries
        }]
    });
}