function http_get(url)
{
	return new Promise(function(resolve, reject){
		try{
			var xmlHttp = new XMLHttpRequest();
		    xmlHttp.onreadystatechange = function() { 
		        if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
		        	if (xmlHttp.responseText.length != 0){
		        		var json = JSON.parse(xmlHttp.responseText);
		            	resolve(json);
		        	}
		    }
		    xmlHttp.open("GET", url, true);
		    xmlHttp.send(null);
		} catch (error){
			reject(error);
		}
	});
}