console.log('loding charts utils');

/*
	data = (name, score) array
*/
function charts_render_tech_performance(containerId, title, data){
	var series = data.map(function(curr){
		return {
			name: curr.name,
			data: [curr.scores.map(x => x.score).reduce(function(prev, curr, idx, array) {
			  return prev.score + curr.score;
			})]
		}
	})

	new Highcharts.Chart({
	    chart: {
	        renderTo: containerId,
	        type: 'bar'
	    },

	    title: {
	    	text: title
	    },

	    credits: {
	    	enabled: false
	    },

	    xAxis: {
	        categories: ['Total Score']
	    },

	    series: series

	});
}

function charts_render_tech_stats(containerId, title, data){
	var series = data.map(function(curr){
		return {
			name: curr.name,
			data: curr.percentage
		}
	})

	new Highcharts.Chart({
		chart: {
			renderTo: containerId,
			type: 'bar'
		},

		title: {
			text: title
		},

		credits: {
			enabled: false
		},

		xAxis: {
			categories: ['Total Score']
		},

		series: series

	});

	console.log(series);
}

function charts_render_score_summary_pie(containerId, title, seriesName, data){
    var container = document.getElementById(containerId);

    var maxItem = data[0];
    data.forEach(item => {
    	item.selected = false;
    	item.sliced = false;

    	if (item.y > maxItem.y){
    		maxItem = item;
    	}
    })

    maxItem.sliced = true;
    maxItem.selected = true;

    var seriesItem = {
    	name: seriesName,
    	colorByPoint: true,
    	data: data
    }

    new Highcharts.Chart({
        chart: {
            renderTo: containerId,
            type: 'pie'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        title: {
            text: title
        },
        credits: {
            enabled: false
        },
        series: [seriesItem]

    });
}

