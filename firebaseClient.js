var Firebase = require('firebase');

var addLibraryScore = function(addTo, lib, addedScore) {
    if (!addTo.languages.JS) {
        addTo.languages.JS = {
            "lines":0,
            "libraries":{}
        }
    }

    if (addTo.languages.JS.libraries[lib]) {
        addTo.languages.JS.libraries[lib].score += addedScore;
    }
    else {
        addTo.languages.JS.libraries[lib] = {"score": addedScore};
    }
};

var addLanguageLines = function(addTo, lang, addedLines) {
    if (addTo.languages[lang]) {
        addTo.languages[lang].lines += addedLines;
    }
    else {
        addTo.languages[lang] = {
            "lines": addedLines,
            "libraries": {}
        }
    }
};

var addTechScore = function(lib, score, name) {
    var ref = new Firebase("https://blazing-heat-8519.firebaseio.com/byTech/" + lib + "/" + name + "/");
    ref.set(score);
};

module.exports.save = function(analyzerResults, name, date) {
    var ref = new Firebase("https://blazing-heat-8519.firebaseio.com/byUser/" + name);

    if (!ref.byTime[date]) {
        ref.byTime[date] = {
            "languages": {}
        };
    }

    for (var lang in analyzerResults.languages) {
        addLanguageLines(ref.total, lang, analyzerResults.languages[lang]);
        addLanguageLines(ref.byTime[date], lang, analyzerResults.languages[lang]);
    }

    for (var lib in analyzerResults.libraries) {
        var score = analyzerResults.libraries[lib];
        addLibraryScore(ref.total, lib, score);
        addLibraryScore(ref.byTime[date], lib, score);
        addTechScore(lib, score, name);
    }
};